package exercicios.salario1;

import salario1.domain.Funcionario;
import salario1.type.Nivel;

public class FuncionarioBuilder {

    Funcionario funcionario;

    public FuncionarioBuilder() {
        funcionario = new Funcionario();
    }

    public FuncionarioBuilder withId(Integer id) {
        funcionario.setId(id);
        return this;
    }

    public FuncionarioBuilder withNome(String nome) {
        funcionario.setNome(nome);
        return this;
    }

    public FuncionarioBuilder withArea(String area) {
        funcionario.setArea(area);
        return this;
    }

    public FuncionarioBuilder withSalario(double salario) {
        funcionario.setSalarioMensal(salario);
        return this;
    }

    public FuncionarioBuilder withNivel(Nivel nivel) {
        funcionario.setNivel(nivel);
        return this;
    }

    public Funcionario build() {
        return funcionario;
    }

}
