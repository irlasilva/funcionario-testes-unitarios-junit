package exercicios.salario1;

import org.junit.Test;
import salario1.domain.Funcionario;
import salario1.regras.CalculaPPR;
import salario1.rh.RHValidator;
import salario1.type.Nivel;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import salario1.domain.Funcionario;
import salario1.exception.PprInvalidaException;
import salario1.regras.CalculaPPR;
import salario1.rh.RHValidator;
import salario1.type.Nivel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CalculaPprMockTest {

    @Test
    public void calculaPPRValida_Pleno() {
        // Arrange
        RHValidator rhValidator = mock(RHValidator.class);
        Funcionario funcionario = new FuncionarioBuilder().withNome("Mockator")
                .withArea("Infra").withSalario(5000).withNivel(Nivel.PLENO).build();

        when(rhValidator.funcionarioAptoAReceberPPR(funcionario)).thenReturn(true);
        CalculaPPR calculaPPR = new CalculaPPR(rhValidator);

        // Act
        double ppr = calculaPPR.retornaValorPPR(funcionario);

        // Assert
        assertEquals(16500, ppr, 0.0);
    }

    @Test
    public void calculaPPRValida_Pleno_Any() {
        // Arrange
        RHValidator rhValidator = mock(RHValidator.class);
        Funcionario funcionario = new FuncionarioBuilder().withNome("Mockator")
                .withArea("Infra").withSalario(5000).withNivel(Nivel.PLENO).build();

        when(rhValidator.funcionarioAptoAReceberPPR(any(Funcionario.class))).thenReturn(true);
        CalculaPPR calculaPPR = new CalculaPPR(rhValidator);

        // Act
        double ppr = calculaPPR.retornaValorPPR(funcionario);

        // Assert
        assertEquals(16500, ppr, 0.0);
    }

    @Test
    public void calculaPprInvalida_Pleno() {
        RHValidator rhValidator = mock(RHValidator.class);
        Funcionario funcionario = new FuncionarioBuilder().withNome("Mockator")
                .withArea("Infra").withSalario(8000).withNivel(Nivel.PLENO).build();

        when(rhValidator.funcionarioAptoAReceberPPR(funcionario)).thenReturn(false);
        CalculaPPR calculaPPR = new CalculaPPR(rhValidator);

        try {
            double ppr = calculaPPR.retornaValorPPR(funcionario);
            fail("PprInvalidaException esperada");
        } catch (PprInvalidaException exc) {
            assertEquals("PPR invalida. Procure a gerencia.", exc.getMessage());
        } catch (Exception e) {
            fail("Exception nao esperada foi retornada");
        }
    }
}
