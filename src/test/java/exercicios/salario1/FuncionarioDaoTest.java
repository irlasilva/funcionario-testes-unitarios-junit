package exercicios.salario1;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import salario1.dao.FuncionarioDao;
import salario1.domain.Funcionario;
import salario1.type.Nivel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FuncionarioDaoTest {

    /**
     * Suponha que FuncionarioDao é algum recurso com tempo muito alto de setup e deve ser reutilizado
     * em todos os testes desta classe.
     */
    private static FuncionarioDao funcionarioDao = new FuncionarioDao();

    @BeforeClass
    public static void populaListaDeFuncionarios() {
        funcionarioDao.inicializaLista();
    }

    @Before
    public void setupPerTest() {
        Funcionario func1 = new FuncionarioBuilder().withId(10).withNome("funcionario1")
                .withArea("area1").withSalario(2000).withNivel(Nivel.JUNIOR).build();
        Funcionario func2 = new FuncionarioBuilder().withId(11).withNome("funcionario2")
                .withArea("area2").withSalario(3100).withNivel(Nivel.GERENTE).build();
        Funcionario func3 = new FuncionarioBuilder().withId(12).withNome("funcionario3")
                .withArea("area3").withSalario(3200).withNivel(Nivel.ESPECIALISTA).build();

        funcionarioDao.addFuncionario(func1);
        funcionarioDao.addFuncionario(func2);
        funcionarioDao.addFuncionario(func3);
    }

    @After
    public void limpaDados() {
        funcionarioDao.limpaDados();
    }

    @Test
    public void testFindByIdRetornaFuncionarioValido() {
        Funcionario funcionario = funcionarioDao.findById(10);

        // assert que o funcionario recuperado possui o nome esperado
        assertEquals("funcionario1", funcionario.getNome());
    }

    @Test
    public void testDeleteByIdComSucesso() {
        funcionarioDao.deleteById(10);
        assertEquals(2, funcionarioDao.contagemDeFuncionarios());
    }

    @Test
    public void testCreateFuncionarioSucesso() {
        // Novo funcionario a ser criado
        Funcionario novoFunc = new FuncionarioBuilder().withNome("novo funcionario").withArea("area 51")
                .withSalario(2100).withNivel(Nivel.GERENTE).build();

        funcionarioDao.create(novoFunc);
        assertNotNull(novoFunc.getId());
        assertEquals(4, funcionarioDao.contagemDeFuncionarios());
    }

    @Test
    public void testUpdateFuncionarioSucesso() {
        Funcionario funcionarioAtualizado = new Funcionario(10, "nomeAtualizado", "novaArea", 3000, Nivel.PLENO);

        funcionarioDao.update(funcionarioAtualizado);

        Funcionario funcionario = funcionarioDao.findById(10);

        assertEquals("nomeAtualizado", funcionario.getNome());
        assertEquals("novaArea", funcionario.getArea());
        assertEquals(Nivel.PLENO, funcionario.getNivel());
    }
}
