package exercicios.salario1;

import org.junit.Test;
import salario1.domain.Funcionario;
import salario1.regras.CalculaPPR;
import salario1.rh.RHValidator;
import salario1.type.Nivel;

import static org.junit.Assert.*;

public class CalculaPPRTest {

    @Test
    public void calculaPPR_Junior() {
        // arrange
        Funcionario funcionario = new FuncionarioBuilder().withNome("Marcelo")
                .withArea("TI").withSalario(3000).withNivel(Nivel.JUNIOR).build();

        // act
        CalculaPPR calculaPPR = new CalculaPPR(new RHValidator());
        double valor = calculaPPR.retornaValorPPR(funcionario);

        //assert
        assertEquals(6300, valor, 0.0);
    }

    @Test
    public void calculaPPR_Pleno() {
        // arrange
        Funcionario funcionario = new FuncionarioBuilder().withNome("Marcelo")
                .withArea("TI").withSalario(4000).withNivel(Nivel.PLENO).build();
        // act

        CalculaPPR calculaPPR = new CalculaPPR(new RHValidator());
        double valor = calculaPPR.retornaValorPPR(funcionario);

        //assert
        assertEquals(13200, valor, 0.0);
    }

    @Test
    public void calculaPPR_Senior() {
        // arrange
        Funcionario funcionario = new FuncionarioBuilder().withNome("Marcelo")
                .withArea("TI").withSalario(5000).withNivel(Nivel.SENIOR).build();
        // act

        CalculaPPR calculaPPR = new CalculaPPR(new RHValidator());
        double valor = calculaPPR.retornaValorPPR(funcionario);

        //assert
        assertEquals(25000, valor, 0.0);
    }

    @Test(expected = RuntimeException.class)
    public void calculaPPR_Nullpointer() {

        CalculaPPR calculaPPR = new CalculaPPR(new RHValidator());
        double valor = calculaPPR.retornaValorPPR(null);
    }

    @Test
    public void calculaPPR_Nullpointer_try() {
        try {
            CalculaPPR calculaPPR = new CalculaPPR(new RHValidator());
            double valor = calculaPPR.retornaValorPPR(null);
            fail("NullPointerException era esperada");
        } catch (RuntimeException npe) {
            assertEquals(npe.getMessage(), "Funcionario nao pode ser null");
        }
    }

}
