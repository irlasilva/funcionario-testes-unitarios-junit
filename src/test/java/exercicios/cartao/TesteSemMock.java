package exercicios.cartao;

import cartao.exception.CartaoException;
import cartao.regras.MasterCard;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TesteSemMock {

    @Test
    public void testeValorParcela() throws CartaoException {
        MasterCard master = new MasterCard();
        assertEquals(10.00, master.calculaParcela(100, 10), 0.0);
    }

    @Test(expected = CartaoException.class)
    public void testeValorMinimoNaoAtingido() throws CartaoException {
        MasterCard master = new MasterCard();
        master.calculaParcela(5, 2);
    }
}
