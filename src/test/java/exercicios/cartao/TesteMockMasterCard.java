package exercicios.cartao;

import cartao.exception.CartaoException;
import cartao.regras.MasterCard;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class TesteMockMasterCard {

    // TODO Crie teste usando um mock do cartao MasterCard valor 100, 10 parcelas.
    @Test
    public void testeMasterCard() throws CartaoException {
        MasterCard master = mock(MasterCard.class);
        when(master.calculaParcela(100, 10)).thenReturn(10.00);

        assertEquals(10.00, master.calculaParcela(100, 10), 0.0);

        verify(master).calculaParcela(100, 10);
    }

    // TODO Crie um teste usando mock que lance excecao com valor abaixo de 50
    @Test(expected = CartaoException.class)
    public void testeValorMinimoNaoAtingido() throws CartaoException {
        MasterCard masterCard = mock(MasterCard.class);
        when(masterCard.calculaParcela(5, 2)).thenThrow(CartaoException.class);

        masterCard.calculaParcela(5, 2);
    }
}
