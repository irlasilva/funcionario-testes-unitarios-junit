package exercicios.crud;

import crud.domain.Produto;

public class ProdutoBuilder {

    private Produto produto;

    public ProdutoBuilder() {
        produto = new Produto();
    }

    public ProdutoBuilder withId(Integer id) {
        produto.setId(id);
        return this;
    }

    public ProdutoBuilder withNome(String nome) {
        produto.setNome(nome);
        return this;
    }

    public ProdutoBuilder withPreco(String preco) {
        produto.setPreco(preco);
        return this;
    }

    public Produto build() {
        return produto;
    }
}
