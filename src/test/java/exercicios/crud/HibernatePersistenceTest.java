package exercicios.crud;

import crud.domain.Produto;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class HibernatePersistenceTest {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    @BeforeClass
    public static void init() {
        entityManagerFactory = Persistence.createEntityManagerFactory("crud-test-persistence-unit");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Before
    public void adicionaDadosNoBancoAntesDeCadaTeste(){
        Produto produto1 = new ProdutoBuilder().withId(1).withNome("camisa polo").withPreco("50.00").build();
        Produto produto2 = new ProdutoBuilder().withId(2).withNome("calca jeans").withPreco("100.00").build();

        entityManager.clear();
        entityManager.getTransaction().begin();
        entityManager.persist(produto1);
        entityManager.persist(produto2);
        entityManager.getTransaction().commit();
    }

    // Operacoes de leitura não precisam iniciar transaction
    @Test
    public void testGetProductById() {
        Produto produto = entityManager.find(Produto.class, 1);
        assertEquals(produto.getPreco(), "50.00");
        assertEquals(produto.getNome(), "camisa polo");
    }

    @Test
    public void testDeletaProduto() {
        Produto produtoADeletar = entityManager.find(Produto.class, 2);

        entityManager.getTransaction().begin();
        entityManager.remove(produtoADeletar);
        entityManager.getTransaction().commit();

        Produto produtoDoBanco = entityManager.find(Produto.class, 2);

        assertNull(produtoDoBanco);
    }

    @Test
    public void testNamedQuery() {
        List<Produto> todosProdutos = entityManager.createNamedQuery("Produto.buscaTodos", Produto.class)
                .getResultList();
        assertEquals(2, todosProdutos.size());
    }

    @After
    public void cleanupDb() {
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("TRUNCATE table Produto").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @AfterClass
    public static void tearDown() {
        entityManager.clear();
        entityManager.close();
        entityManagerFactory.close();
    }
}
