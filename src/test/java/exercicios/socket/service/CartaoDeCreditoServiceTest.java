package exercicios.socket.service;

import org.junit.Test;
import socket.client.MasterCardSocketClient;
import socket.domain.CartaoDeCredito;
import socket.domain.OperacaoDeCartao;
import socket.repositorios.RepositorioOperacoes;
import socket.service.CartaoDeCreditoService;
import socket.type.TipoOperacao;

import java.io.IOException;
import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CartaoDeCreditoServiceTest {

    // TODO Exercicio: Refatorar CartaoDeCreditoService de modo a ser possivel usar um mock pro socket
    @Test
    public void testeRealizaOperacao() throws IOException {
        MasterCardSocketClient socketMock = mock(MasterCardSocketClient.class);
        CartaoDeCreditoService cartaoService = new CartaoDeCreditoService(new RepositorioOperacoes(), socketMock);

        when(socketMock.comunicaOperadora(any(OperacaoDeCartao.class))).thenReturn("Sucesso");

        CartaoDeCredito cartaoDeCredito = new CartaoDeCredito();
        cartaoDeCredito.setNumero(123456L);
        cartaoDeCredito.setNomeProprietario("nome");

        cartaoService.realizaOperacao(cartaoDeCredito, TipoOperacao.CREDITO, new BigDecimal("100"));
    }
}
