package socket.repositorios;

import socket.domain.OperacaoDeCartao;
import socket.exception.ObjetoNaoEncontradoException;
import socket.exception.OperacaoIlegalException;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class RepositorioOperacoes {

    private List<OperacaoDeCartao> operacaoDeCartaoList;
    private Long sequencia = 1000L;

    public RepositorioOperacoes() {
        this.operacaoDeCartaoList = new ArrayList<OperacaoDeCartao>();
    }

    public RepositorioOperacoes(List<OperacaoDeCartao> operacaoDeCartaoList) {
        this.operacaoDeCartaoList = operacaoDeCartaoList;
    }

    public void salvaOperacaoCartao(OperacaoDeCartao operacaoDeCartao) {
        if (operacaoDeCartao.getId() == null) {
            operacaoDeCartao.setId(sequencia++);
            operacaoDeCartaoList.add(operacaoDeCartao);

        } else {
            OperacaoDeCartao dbOperacao = buscaOperacaoPorId(operacaoDeCartao.getId());
            if (dbOperacao == null) {
                throw new ObjetoNaoEncontradoException("Operacao nao encontrada para atualizacao");
            }

            if (!dbOperacao.getTipoOperacao().equals(operacaoDeCartao.getTipoOperacao()) ||
                    !dbOperacao.getCartao().equals(operacaoDeCartao.getCartao())) {
                throw new OperacaoIlegalException("Propriedade nao atualizavel");
            }

            dbOperacao.setValor(operacaoDeCartao.getValor());
        }
    }

    public OperacaoDeCartao buscaOperacaoPorId(Long id) {
        for (OperacaoDeCartao operacao : operacaoDeCartaoList) {
            if (operacao.getId().equals(id)) {
                return operacao;
            }
        }
        return null;
    }

    public void deletaPorId(Long id) {
        ListIterator<OperacaoDeCartao> iterator = operacaoDeCartaoList.listIterator();
        while(iterator.hasNext()) {
            if (iterator.next().getId().equals(id)) {
                iterator.remove();
                break;
            }
        }
    }

}
