package socket.domain;

import socket.type.TipoOperacao;

import java.math.BigDecimal;

public class OperacaoDeCartao {

    private Long id;
    private CartaoDeCredito cartao;
    private TipoOperacao tipoOperacao;
    private BigDecimal valor;

    public OperacaoDeCartao(CartaoDeCredito cartao, TipoOperacao tipoOperacao) {
        this.cartao = cartao;
        this.tipoOperacao = tipoOperacao;
    }

    public OperacaoDeCartao(CartaoDeCredito cartao, TipoOperacao tipoOperacao, BigDecimal valor) {
        this.cartao = cartao;
        this.tipoOperacao = tipoOperacao;
        this.valor = valor;
    }

    public OperacaoDeCartao() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CartaoDeCredito getCartao() {
        return cartao;
    }

    public void setCartao(CartaoDeCredito cartao) {
        this.cartao = cartao;
    }

    public TipoOperacao getTipoOperacao() {
        return tipoOperacao;
    }

    public void setTipoOperacao(TipoOperacao tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
