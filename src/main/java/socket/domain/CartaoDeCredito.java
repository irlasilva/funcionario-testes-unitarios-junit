package socket.domain;

import java.util.Date;

public class CartaoDeCredito {

    private Long numero;
    private String nomeProprietario;
    private Date vencimento;

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getNomeProprietario() {
        return nomeProprietario;
    }

    public void setNomeProprietario(String nomeProprietario) {
        this.nomeProprietario = nomeProprietario;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    @Override
    public String toString() {
        return "CartaoDeCredito{" +
                "numero=" + numero +
                ", nomeProprietario='" + nomeProprietario + '\'' +
                ", vencimento=" + vencimento +
                '}';
    }
}
