package socket.service;

import socket.client.MasterCardSocketClient;
import socket.domain.CartaoDeCredito;
import socket.domain.OperacaoDeCartao;
import socket.exception.OperacaoRejeitadaException;
import socket.repositorios.RepositorioOperacoes;
import socket.type.TipoOperacao;

import java.io.IOException;
import java.math.BigDecimal;

public class CartaoDeCreditoService {

    private RepositorioOperacoes repositorioOperacoes;
    private MasterCardSocketClient socketClient;

    public CartaoDeCreditoService(RepositorioOperacoes repositorioOperacoes, MasterCardSocketClient socketClient) {
        this.repositorioOperacoes = repositorioOperacoes;
        this.socketClient = socketClient;
    }

    public void realizaOperacao(CartaoDeCredito cartao, TipoOperacao tipoOperacao, BigDecimal valor) throws IOException {
        OperacaoDeCartao operacao = new OperacaoDeCartao(cartao, tipoOperacao, valor);

        boolean operacaoValida = verificaSeOperacaoEValida(operacao);

        if (operacaoValida) {
            String resultado = socketClient.comunicaOperadora(operacao);

            if (resultado.equals("Sucesso")) {
                repositorioOperacoes.salvaOperacaoCartao(operacao);
            } else {
                throw new OperacaoRejeitadaException("Operacao rejeitada pela operadora do cartao");
            }
        } else {
            throw new OperacaoRejeitadaException("Operacao invalida");
        }
    }

    private boolean verificaSeOperacaoEValida(OperacaoDeCartao operacao) {
        if (operacao.getTipoOperacao().equals(TipoOperacao.AUTORIZACAO) &&
                operacao.getValor() != null) {
            return false;
        } else if (operacao.getTipoOperacao().equals(TipoOperacao.CREDITO) &&
                operacao.getValor() == null) {
            return false;
        } else if (operacao.getTipoOperacao().equals(TipoOperacao.DEBITO) &&
                operacao.getValor() == null) {
            return false;
        }

        return true;
    }

}
