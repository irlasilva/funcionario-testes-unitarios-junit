package socket.type;

public enum TipoOperacao {

    DEBITO(1, "Debito"),
    CREDITO(2, "Credito"),
    AUTORIZACAO(3, "Autorizacao");

    int codigoOperacao;
    String nomeOperacao;

    TipoOperacao(int codigoOperacao, String nomeOperacao) {
        this.codigoOperacao = codigoOperacao;
        this.nomeOperacao = nomeOperacao;
    }

    public int getCodigoOperacao() {
        return codigoOperacao;
    }

    public String getNomeOperacao() {
        return nomeOperacao;
    }
}
