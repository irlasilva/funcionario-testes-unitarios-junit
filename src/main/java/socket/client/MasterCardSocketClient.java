package socket.client;

import socket.domain.OperacaoDeCartao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MasterCardSocketClient {

    private final int porta = 8888;

    public String comunicaOperadora(OperacaoDeCartao operacaoDeCartao) throws IOException {
        Socket socket = new Socket("127.0.0.1", porta);
        PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        output.print(operacaoDeCartao);
        String resposta = input.readLine();

        output.close();
        input.close();
        socket.close();

        return resposta;
    }
}
