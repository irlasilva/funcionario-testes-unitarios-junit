package socket.exception;

public class OperacaoRejeitadaException extends RuntimeException {

    public OperacaoRejeitadaException(String message) {
        super(message);
    }
}
