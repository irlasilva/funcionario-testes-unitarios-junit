package socket.exception;

public class OperacaoIlegalException extends RuntimeException {

    public OperacaoIlegalException(String message) {
        super(message);
    }
}
