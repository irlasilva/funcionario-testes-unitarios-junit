package cartao.regras;

import cartao.exception.CartaoException;
import cartao.interfaces.Cartao;

public class MasterCard implements Cartao {

    public double calculaParcela(double valor, int parcelas) throws CartaoException {
        double valorParcela = 0;

        if (valor < 50.00) {
            throw new CartaoException("Valor mínimo não atingido");
        } else {
            valorParcela = valor / parcelas;
        }

        return valorParcela;
    }
}
