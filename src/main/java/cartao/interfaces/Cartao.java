package cartao.interfaces;

import cartao.exception.CartaoException;

public interface Cartao {

    double calculaParcela(double valor, int parcelas) throws CartaoException;
}
