package salario1.rh;

import salario1.domain.Funcionario;
import salario1.regras.CalculaPPR;

public class RHValidator {

    public boolean funcionarioAptoAReceberPPR(Funcionario funcionario) {
        return (!funcionarioTemPendencias(funcionario) && !pprAcimaDoTeto(funcionario));
    }

    // Nomes começando de A a L, minúsculo ou maiúsculo, tem pendencias.
    private boolean funcionarioTemPendencias(Funcionario funcionario) {
        if (funcionario.getNome().matches("([a-lA-L]).*")) {
            return true;
        }
        return false;
    }

    // indica se a PPR para o funcionario é acima do teto para o nível.
    public boolean pprAcimaDoTeto(Funcionario funcionario) {
        double teto = 0;
        switch (funcionario.getNivel()) {
            case JUNIOR:
                teto = 15000;
                break;
            case PLENO:
                teto = 20000;
                break;
            case SENIOR:
                teto = 30000;
                break;
            case ESPECIALISTA:
                teto = 40000;
                break;
            case GERENTE:
                teto = 50000;
                break;
        }
        CalculaPPR calculaPPR = new CalculaPPR(new RHValidator());
        double pprFuncionario = calculaPPR.calculaPprParaNivel(funcionario.getNivel(),
                funcionario.getSalarioMensal());

        return (pprFuncionario > teto);
    }
}
