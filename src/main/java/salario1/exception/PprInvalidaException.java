package salario1.exception;

public class PprInvalidaException extends RuntimeException {
    public PprInvalidaException(String message) {
        super(message);
    }
}
