package salario1.dao;

import salario1.domain.Funcionario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FuncionarioDao {

    private List<Funcionario> funcionarioList;
    private int sequencia = 1000;

    public FuncionarioDao() {
    }

    public void inicializaLista() {
        if (funcionarioList == null) {
            funcionarioList = new ArrayList<Funcionario>();
        }
    }

    public void addFuncionario(Funcionario funcionario) {
        funcionarioList.add(funcionario);
    }

    public void limpaDados() {
        funcionarioList.clear();
    }

    public int contagemDeFuncionarios() {
        return funcionarioList.size();
    }

    public void create(Funcionario funcionario) {
        if (funcionario.getId() != null) {
            throw new RuntimeException("Funcionario ja existe.");
        }
        funcionario.setId(sequencia++);
        funcionarioList.add(funcionario);
    }

    public void deleteById(Integer id) {
        Iterator<Funcionario> iterator = funcionarioList.listIterator();
        boolean funcionarioEncontrado = false;
        while (iterator.hasNext()) {
            if (iterator.next().getId().equals(id)) {
                iterator.remove();
                funcionarioEncontrado = true;
                break;
            }
        }
        if (!funcionarioEncontrado) {
            throw new RuntimeException("Funcionario nao encontrado.");
        }
    }

    public void update(Funcionario funcionario) {
        Iterator<Funcionario> iterator = funcionarioList.listIterator();
        boolean funcionarioEncontrado = false;

        while (iterator.hasNext()) {
            Funcionario dbFuncionario = iterator.next();
            if (dbFuncionario.getId().equals(funcionario.getId())) {
                dbFuncionario.setArea(funcionario.getArea());
                dbFuncionario.setNome(funcionario.getNome());
                dbFuncionario.setNivel(funcionario.getNivel());
                dbFuncionario.setSalarioMensal(funcionario.getSalarioMensal());
                funcionarioEncontrado = true;
                break;
            }
        }

        if (!funcionarioEncontrado) {
            throw new RuntimeException("Funcionario nao encontrado");
        }
    }

    public Funcionario findById(Integer id) {
        Iterator<Funcionario> iterator = funcionarioList.listIterator();

        while (iterator.hasNext()) {
            Funcionario dbFuncionario = iterator.next();
            if (dbFuncionario.getId().equals(id)) {
                return dbFuncionario;
            }
        }

        throw new RuntimeException("Funcionario not found.");
    }
}
