package salario1.domain;

import salario1.type.Nivel;

public class Funcionario {

    private Integer id;
    private String nome;
    private String area;
    private double salarioMensal;
    private Nivel nivel;

    public Funcionario() {}

    public Funcionario(Integer id, String nome, String area, double salarioMensal, Nivel nivel) {
        this.id = id;
        this.nome = nome;
        this.area = area;
        this.salarioMensal = salarioMensal;
        this.nivel = nivel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public double getSalarioMensal() {
        return salarioMensal;
    }

    public void setSalarioMensal(double salarioMensal) {
        this.salarioMensal = salarioMensal;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    @Override
    public String toString() {
        return "Funcionario{" +
                "nome='" + nome + '\'' +
                ", area='" + area + '\'' +
                ", salarioMensal=" + salarioMensal +
                ", nivel=" + nivel +
                '}';
    }

}
