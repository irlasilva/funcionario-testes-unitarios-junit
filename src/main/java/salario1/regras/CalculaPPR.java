package salario1.regras;

import salario1.domain.Funcionario;
import salario1.exception.PprInvalidaException;
import salario1.rh.RHValidator;
import salario1.type.Nivel;

public class CalculaPPR {

    private RHValidator rhValidator;

    public CalculaPPR(RHValidator rhValidator) {
        this.rhValidator = rhValidator;
    }

    public double retornaValorPPR(Funcionario funcionario) {
        if (funcionario == null) {
            throw new RuntimeException("Funcionario nao pode ser null");
        }

        if (rhValidator.funcionarioAptoAReceberPPR(funcionario)) {
            return calculaPprParaNivel(funcionario.getNivel(), funcionario.getSalarioMensal());
        } else {
            throw new PprInvalidaException("PPR invalida. Procure a gerencia.");
        }
    }

    // Este método é usado por CalculaPPR e RHValidator.
    public double calculaPprParaNivel(Nivel nivel, double salario) {
        return (nivel.getFatorDeMultiplicacao() * salario);
    }
}
